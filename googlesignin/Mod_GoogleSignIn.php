<?php


namespace Zotlabs\Module;

use App;
use Zotlabs\Lib\Apps;
use Zotlabs\Web\Controller;

// only supports POST with IDtoken from Google
class GoogleTokenSignIn extends Controller {

	function post() {

		// Is this a Redirect from Google as we instructed to, then idtoken parameter is given
		if ( x( $_POST, 'idtoken' ) ) {
			$idtoken = $_POST['idtoken'];

			logger('Google Token sign-in: idtoken is present', LOGGER_DEBUG);
			$clientID = get_config('googlesignin', 'clientID');

			require_once __DIR__ . '/vendor/autoload.php';

			$client = new \Google_Client(['client_id' => $clientID]); 
			$payload = $client->verifyIdToken($idtoken);
			if ($payload) {
				$email = $payload['email'];

				logger('Google Token Sign-in: valid IDtoken, email = '. print_r($email, true), LOGGER_DEBUG);
				
				// all ok, continue as logged in user
				$r = q("select * from account where account_email = '%s' LIMIT 1", $email);

				if (count($r)) {
					$record = $r[0];

					logger('Account: ' . print_r($record, true), LOGGER_TRACE);

					$channel_id = $record['account_default_channel'];
				        $_SESSION['uid'] = $channel_id;
					
					require_once('include/security.php');
					authenticate_success($record, null, true, false, true, true);
				
					// return JSON to be parsed by XHR on load
					echo('{"account_email":"'. $email .'","URL":"'. z_root() .'"}');
					killme();

				} else {
					logger('Google Token Sign-in: email '. print_r($email, true) . ' not found in HZ DB', LOGGER_DEBUG);
					header('HTTP/1.0 401 Unauthorized');
	                                echo('{"error_message":"'. t('Google sign-in requires a Gmail address that is registered in Hubzilla'). '"}');
					killme();
				}

			} else {
	  		        // Invalid ID token
				logger('Google Token Sign-in: invalid IDtoken!', LOGGER_DEBUG);
				
				header('HTTP/1.0 401 Unauthorized');
                                echo('{"error_message":"'. t('Google sign-in verification failed'). '"}');
				killme();		
			}	  

		}
		else {
			logger('Google Token Sign-in: no idtoken present!', LOGGER_DEBUG);
		}
	}

}

// Google Sign in App configuration
class GoogleSignin extends Controller {

	function get() {
		if(! local_channel())
			return;

		if(! Apps::addon_app_installed(local_channel(), 'googlesignin')) {
			//Do not display any associated widgets at this point
			App::$pdl = '';

			$o = '<b>Google Sign-in App (Not Installed):</b><br>';
			$o .= t('Sign in to Hubzilla using your Google account');
			return $o;
		}

		$content .= '<div class="section-content-info-wrapper">';
		$content .= t('Google Sign-in');
		$content .= '</div>';

                $clientID = get_config('googlesignin','clientID');

		$content .= '<div class="section-content-info-wrapper">';
		$content .= t('This app enables a Google sign-in button on the login page.<br>(To be implemented: If no Google clientID is configured, the sign-in button will be greyed out.)<br>');
		$content .= t('
<br><b>To create a Google client ID:</b>
<li>Follow this <a href=https://developers.google.com/identity/sign-in/web/sign-in>link</a></li>
<li>Click the button "Configure a project"</li>
<li>Follow the instructions:</li>
<li>Enter e.g. "Hubzilla" as project name</li>
<li>Enter e.g. "Hubzilla" as product name</li>
<li>For the scope, select "Web browser"</li>
<li>For "Authorized Javascript Origin", enter your hub address e.g. https://myhub.tld</li>
<li>Follow the link to the <a href=https://console.developers.google.com>Google developer console</a></li>
<li>Go to "APIs & services" > "Credentials"</li> 
<li>Edit the "OAuth client"</li>
<li>In "Restrictions" > "Authorized JavaScript origins", it should have your hub address "https://myhub.tld".</li>
<li>In "Restrictions" > "Authorized redirect URIs", add the redirect URI "https://myhub.tld/googletokensignin".</li>
<li>Press Save.</li>
<li>Copy the OAuth clientID into the app configuration below & submit</li>
');
                $content .= '</div>';

		$content .= replace_macros(get_markup_template('field_input.tpl'), 
			[
				'$field' => ['clientID', t('Enter your Google clientID'), $clientID, t('Word')]
			]
		);
 
		
		$tpl = get_markup_template("settings_addon.tpl");

		$o = replace_macros($tpl, array(
			'$action_url' => 'googlesignin',
			'$form_security_token' => get_form_security_token("googlesignin"),
			'$title' => t('Google Sign-in'),
			'$content'  => $content,
			'$baseurl'   => z_root(),
			'$submit'    => t('Submit'),
		));
		
		return $o;
	}
	
	function post() {

		if(! local_channel())
			return;

		if(! Apps::addon_app_installed(local_channel(),'googlesignin'))
			return;

		check_form_security_token_redirectOnErr('googlesignin', 'googlesignin');

		// saving hub-specific clientID setting
		if (x($_POST, 'clientID')) 
			set_config('googlesignin', 'clientID', $_POST['clientID']);

		info( t('Google Sign-in Settings saved.') . EOL);
	}
}

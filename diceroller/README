Dice Roller - Add-on for Hubzilla

Adds dice rolling, e.g. for usage in table-top gaming in Hubzilla 

Installation
  * Hub admin must install Dice Roller add-on
Note: this enables the feature for all users on the hub
Note: the number of dice is limited to 10 to prevent excessive content generation.

Usage
  * Create a new post/comment of which the text has the roll format, starting with '%roll'.

Usage example
  * Create a post or comment with text "%roll 3d6" => makes a roll, 3 dice, each 6 faces
  After sharing the post/comment, it will be updated with the roll output and result

Features
  * Adds timestamp in the result (prevents duplicate cancelling of posts/comments)
  * Resulting roll post/comment is read-only and cannot be deleted (no cheating)
  * Supported types of games:
    * Default:
      * format: XdA, where X = number of dice, A = number of faces for all dice
      * what: classic dice: each dice has a number of faces counting 1 -> #faces
      * result: the sum of faces of all dice.
    * Fudge:
      * format: XdF, where X = number of dice. "F" indicates Fudge game
      * what: each dice results in '+', '-' or '_' (blank)
      * result: sum is counted as '+1' for '+', '-1' for '-' and '0' for '_'
  * Shortcut '%roll' means 'roll 1 dice, 6 faces'
  * If the number of dice is omitted, it defaults to '1'
